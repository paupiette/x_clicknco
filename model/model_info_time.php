<?php

//récupération de l'adresse et du numéro de tel dans la bdd
function info_farm_query(){
    global $db;

    $requete=$db->query('SELECT * FROM info_farm');

    $tuples = $requete->fetch();

    $requete->closeCursor();

    return $tuples;
}

$info_farm =info_farm_query();
//print_r($info_farm);

//récupération des horaires dans la bdd
function time_hours_query(){
    
    global $db;

    $requete=$db->query('SELECT * FROM time_hours');
    
    $tuples = $requete->fetchAll();
    
    $requete->closeCursor();
    
    return $tuples;
}

$hours =time_hours_query();
?>


