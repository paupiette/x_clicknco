<?php
    //TODO : penser à prévenir les clients lorsqu'un produit de leurs commande n'est plus disponible car définitivement supprimé. 
    function query_delete_product($delete){
        global $db;

        //requête de suppression des lignes dans la table product_order : clé étrangère
        $requete_product_order = "DELETE FROM product_order WHERE id_product = :id_product";

        $requetePO = $db->prepare($requete_product_order);
        $id_product = $delete;
        $requetePO->bindValue(':id_product', $id_product);
        $requetePO->execute();
        $requetePO->closeCursor();


        //requete de suppression du produit
        $requete_product = "DELETE FROM `product` WHERE `id` = :id";
        $requete = $db->prepare($requete_product);
        $id = $delete;
        $requete->bindValue(':id', $id);
        $requete->execute();

        $requete->closeCursor();
    }

    if(isset($_POST['delete'])){
        $delete = $_POST['delete'];
        query_delete_product($delete);
        unset($_POST['delete']);
    }
