<?php

//récupération des produits dans la bdd
function product_query(){
    global $db;

    $requete=$db->query('SELECT * FROM product');

    $tuples = $requete->fetchAll();

    $requete->closeCursor();

    return $tuples;
}

$products = product_query();

?>

