<?php

    function insert_order_query($date, $firstName, $lastName, $email, $phone, $order){
        global $db;
        
        //insertions des données du formulaire dans la table one_order
        $reqInsertOneOrder='
            INSERT INTO one_order (order_status, day_order, client_first_name, client_last_name, client_mail, client_phone)
            VALUES(:order_status, :day_order, :client_first_name, :client_last_name, :client_mail, :client_phone)
        ';

        //préparation de la requête
        $requete = $db->prepare($reqInsertOneOrder);
        $requete->bindValue(':order_status', 'validate');
        $requete->bindValue(':day_order', $date);
        $requete->bindValue(':client_first_name', $firstName);
        $requete->bindValue(':client_last_name', $lastName);
        $requete->bindValue(':client_mail', $email);
        $requete->bindValue(':client_phone', $phone);

        //exécution de la requête
        $requete->execute(); 
        
        
        //récupéréation du dernier id de la table one_order (donc de la ligne que l'on vient de créer)
        $reqOneOrder=$db->query('SELECT @last_id := MAX(id) FROM one_order');
        $LastIdOneOrder = $reqOneOrder->fetch();
        $id_order = $LastIdOneOrder[0];

        $reqOneOrder->closeCursor();

        //insertions des données prevenant du sessionStorage dans la table product_order
        for($i=0; $i<count($order); $i++){
            $quantity = intval($order[$i]['quantity']);
            $id_product = intval($order[$i]['id']);

            $reqInsertProductOrder='
            INSERT INTO product_order (quantity, id_order, id_product)
            VALUES(:quantity, :id_order, :id_product)
            ';

            //préparation de la requête
            $req = $db->prepare($reqInsertProductOrder);
            $req->bindValue(':quantity', $quantity);
            $req->bindValue(':id_order', $id_order);
            $req->bindValue(':id_product', $id_product);

            //exécution de la requête
            $req->execute(); 

            $req->closeCursor();
        }
    
        $requete->closeCursor();

    }

    //utilisation de la fonction insert_order_query()
    if(isset($_POST['first-name'])){
        insert_order_query($date, $firstName, $lastName, $email, $phone, $order);
        //je vide la variable $_POST afin de la réutiliser dans la même session
        unset($_POST);
    }
    
    
    	