<?php

function connexionBD($dbname){
	// paramètres de la base de donnée
	$sgbdname='mysql';
	$host='localhost';
	$charset='utf8';
		
	// dsn : data source name, la source de la base de données 
	$dsn = $sgbdname.':host='.$host.';dbname='.$dbname.';charset='.$charset;

	// utilisateur connecté à la base de donnée
	$username = 'claire';
	$password = 'azeqsd';

	// pour avoir des erreurs SQL actionner dans le bloc catch{}
	$erreur = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

	//try{} catch{} : système de gestion des erreurs
	try {
		/* connexion à la base de données : new PDO créeation d'un nouvel 
		objet PDOqui se connectera à ma base de donnée*/
		$db = new PDO($dsn, $username, $password, $erreur);
		return $db;
	} catch (PDOException $e) {
		die ('Connexion échouée : ' . $e->getMessage() );
		return NULL;
	}
	/* Si le code contenu dans le bloc try{} possède une erreur, alors on
	execute le code dans le bloque catch{} */	
}

$db = connexionBD('clicknco');