<img src="<?php echo $products[$i]['picture_product']; ?>" alt="image du produit">
<div class="content-card">
    <div class="price-h-card">
        <h2><?php echo $products[$i]['name_product']; ?></h2>
        <div class="price"><?php echo $products[$i]['price_product']; ?>€ le kg</div>
    </div>
    <form class="form-card">
        <input class="number" type="number" min="0" max="2000" step="100" placeholder="grammes">
        <button class="button_shop" id="<?php echo $products[$i]['id']; ?>" type="button">ajouter au panier</button>
    </form>
</div>

