//Panier-ajout de cartes////////////////////////////////////////////////////////////

//Préparation
function fillBasket(){
    if(sessionStorage.key(0) != null){
        //on vide le panier avant de le reremplir
        $("#products-basket").empty();
        var somme = 0; 
        for (var i = 0; i < sessionStorage.length; i++) {

            var obj = JSON.parse(sessionStorage.getItem(sessionStorage.key(i)));

            //ajout de div pour bloquer la carte si le produit est dans le panier
            var buttonAdd = document.getElementById(obj.id);
            var cardAdd = buttonAdd.parentNode.parentNode.parentNode;

            function addBasketStyle(){
                var node1 = cardAdd.querySelector('img');
                var divBlocking = document.createElement('div');
                divBlocking.classList.add("add-basket");
                cardAdd.insertBefore(divBlocking, node1);

                var div1 = document.createElement('div');
                div1.innerText = "ajouté";

                var div2 = document.createElement('div');
                div2.innerText = "au panier";

                divBlocking.appendChild(div1);
                divBlocking.appendChild(div2);
            }

            //pour éviter que les div .add_basket s'ajoutent les unes par dessus les autres 
            if(cardAdd.getElementsByClassName('add-basket')[0] == cardAdd.childNodes[1]){
                cardAdd.removeChild(cardAdd.childNodes[1]);
                addBasketStyle();
            }else{
                addBasketStyle();
            }
            
            //carte dans le panier
            var divProduct = document.createElement('div');
            divProduct.classList.add('product-basket');
            basket.appendChild(divProduct);
            
            var pictureProduct = document.createElement('img');
            pictureProduct.setAttribute('src', obj.picture);
            divProduct.appendChild(pictureProduct);

            var divInfo = document.createElement('div');
            divInfo.classList.add('info-basket');
            divProduct.appendChild(divInfo);
            
            var nameProduct = document.createElement('h2');
            nameProduct.textContent = obj.name;
            divInfo.appendChild(nameProduct);

            var divQuantity = document.createElement('div');
            divQuantity.textContent = obj.quantity + ' gr';
            divInfo.appendChild(divQuantity);


            //calcul du prix total dans le panier
            somme = somme + parseFloat(obj.result);

        }

        //pour afficher les entier et 2 décimales après la virgule
        priceTotal = somme.toFixed(2);
        
        divTotal.innerText = priceTotal.toString();    
    }
}





var basket = document.getElementById('products-basket');

var pricesProducts = [];

var priceTotal = 0;

var divTotal = document.getElementById('total-order');







//Utilisation / actions
fillBasket();

//JQUERY récupération type="button" et ajout de la fonction suivante à l'évenement click
$("button").click(function() {
  
    //récupération de l'id
    var recoverId = this.id; 
    
    /////////////////////////////////////////////////
    //récupération des valeurs dans la carte
    //le bouton
    var button = document.getElementById(recoverId);
    //carte
    var card = button.parentNode.parentNode.parentNode;
    //récupération de la valeur de l'input
    var quantity = card.childNodes[3].childNodes[3].childNodes[1].value ;
    //source de l'image
    var picture = card.querySelector('img').src;
    //nom du produit
    var nameProduct = card.childNodes[3].childNodes[1].childNodes[1].textContent;
    //prix du produit au kg --> laisse en string pour le rentrer dans le localstorage
    var pricePerKg = card.childNodes[3].childNodes[1].childNodes[3].textContent;
    var pricePerKgFloat = pricePerKg.replace("€ le kg", "");
    
    //si clic sans avoir de valeur à l'input 
    if (quantity == ""){
        var inputNumber = card.childNodes[3].childNodes[3].childNodes[1];
        inputNumber.classList.remove("number");
        inputNumber.classList.add("add-quantity");
        inputNumber.setAttribute('placeholder', "choisir une quantité");

        $(inputNumber).click(function() {
            inputNumber.classList.remove("add-quantity");
            inputNumber.classList.add("number");
            inputNumber.setAttribute("placeholder", "grammes");
        });
    
    }

    //calcul du prix du produit à la quantité 
    var quantityProduct = parseInt(quantity);
    var priceProduct = parseFloat(pricePerKgFloat);
    var result = (quantityProduct * priceProduct) / 1000;

    //clic dès qu'il y a une valeur d'input sup à 0
    if(0 < parseInt(quantity)){
    
        var product = {  
        "id": recoverId,
        "name" : nameProduct,
        "picture" : picture,
        "quantity" : quantity,
        "result" : result};
        
        sessionStorage.setItem(recoverId , JSON.stringify(product));
    }

    fillBasket();
    
});


