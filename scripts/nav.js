//TODO : ajouter un time pour qu'il mette tout seul le menu burger sans rechargerla page
var d = document.documentElement;
var width_view = d.clientWidth;

var nav_principale = document.getElementById('nav-principale');

var div_nav_principale = document.getElementById('div-nav-principale');

if(width_view < 768){

    //enlever ul
    nav_principale.removeChild(div_nav_principale);

    //création de l'icone de menu burger
    var menu_burger_icon = document.createElement('div');
    menu_burger_icon.classList.add('menu-icon');
    menu_burger_icon.setAttribute('onclick', 'deploiment()');
    nav_principale.appendChild(menu_burger_icon);

    var span_menu_burger_icon = document.createElement('span');
    menu_burger_icon.appendChild(span_menu_burger_icon);

    var basket_icon = document.getElementById('a-basket');
}else{}

var div_nav_menu_burger;

function deploiment(){
    menu_burger_icon.classList.add('is-opened');
    menu_burger_icon.setAttribute('onclick', 'fermeture()');

    div_nav_menu_burger = document.createElement('div');
    div_nav_menu_burger.classList.add('div-nav-menu-burger');
    div_nav_menu_burger.setAttribute('id', 'div-nav-menu-burger');
    nav_principale.appendChild(div_nav_menu_burger);

    var ul_nav_menu_burger = document.createElement('ul');
    div_nav_menu_burger.appendChild(ul_nav_menu_burger);

    var li_accueil_ul = document.createElement('li');
    li_accueil_ul.innerHTML ='<a href = "index_switch.php?controler_home=set">accueil</a>';
    ul_nav_menu_burger.appendChild(li_accueil_ul);

    var li_potager_ul = document.createElement('li');
    li_potager_ul.innerHTML ='<a href = "index_switch.php?controler_shop=set">boutique</a>';
    ul_nav_menu_burger.appendChild(li_potager_ul);

    //pour que l'icon du panier suive l'ouverture du menu burger
    if(basket_icon !== null){
    basket_icon.classList.remove('a-basket');
    basket_icon.classList.add('a-basket-deployed');
    }else{}
}

function fermeture(){
    menu_burger_icon.classList.remove('is-opened');
    nav_principale.removeChild(div_nav_menu_burger);
    menu_burger_icon.setAttribute('onclick', 'deploiment()');

    //pour que l'icon du panier suive la fermeture du menu burger
    if(basket_icon !== null){
    basket_icon.classList.remove('a-basket-deployed');
    basket_icon.classList.add('a-basket');
    }else{}
}