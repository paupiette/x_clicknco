
<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

// On demarre les sessions
session_start();  

date_default_timezone_set('Europe/Paris');

include('model/config_db.php');


//Accueil 
if (isset($_GET['controler_home'])) {
	include('controler/controler_home.php');
}

//Boutique
if (isset($_GET['controler_shop'])) {
	include('controler/controler_shop.php');
}

//Espace administrateur
if (isset($_GET['products_admin'])) {
	include('controler/controler_products_admin.php');//TODO modifier
}

if(isset($_GET['product_delete_admin'])){
	include('controler/controler_product_delete_admin.php');
}




//page test upload images et redimension avant entrée en bdd
if (isset($_GET['img'])){
	include('view/admin/testimg.php');
}
?>

