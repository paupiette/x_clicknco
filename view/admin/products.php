<?php
        include('include/head.php');
    ?>
    <!--JQUERY-->
    <script src="https://code-jquery-com.translate.goog/jquery-3.6.0.js?_x_tr_sl=auto&_x_tr_tl=fr&_x_tr_hl=fr&_x_tr_pto=ajax,elem"></script>
    <link href="styles/card.css" rel="stylesheet">
</head>
<body>
<?php
    include('include/nav.php');
?>

    <div class="contain-general-garden">
        <main>
            
            <?php
                for($i=0; $i < count($products); $i++){
            ?>
                        <div class="card">
                            <img src="<?php echo $products[$i]['picture_product']; ?>" alt="image du produit">
                            <div class="content-card">
                                <div class="price-h-card">
                                    <h2><?php echo $products[$i]['name_product']; ?></h2>
                                    <div class="price"><?php echo $products[$i]['price_product']; ?>€ le kg</div>
                                </div>
                                <form action="index_switch.php?product_delete_admin=set" method="post" class="form-card">
                                    <input name="delete" type="hidden" value="<?php echo $products[$i]['id']; ?>">
                                    <button class="button_delete" type="submit">supprimer le produit</button>
                                </form>
                            </div>
                        </div>
                        <?php
                }
                ?>
            
        </main>
    </div>
<?php
    include('include/footer.php');
?>