    <?php
        include('include/head.php');
    ?>
    <!--JQUERY-->
    <script src="https://code-jquery-com.translate.goog/jquery-3.6.0.js?_x_tr_sl=auto&_x_tr_tl=fr&_x_tr_hl=fr&_x_tr_pto=ajax,elem"></script>
    <link href="styles/shop/anchor.css" rel="stylesheet">
    <link href="styles/card.css" rel="stylesheet">
    <link href="styles/shop/basket.css" rel="stylesheet">
    <link href="styles/shop/popup.css" rel="stylesheet">
</head>
<body>
<?php
    include('include/nav.php');
?>

    <div class="a-basket" id="a-basket">
        <a href="#div-basket">
            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-basket" viewBox="0 0 16 16">
                <path d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1v4.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 13.5V9a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h1.217L5.07 1.243a.5.5 0 0 1 .686-.172zM2 9v4.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V9H2zM1 7v1h14V7H1zm3 3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 4 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 6 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 8 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5z"/>
            </svg>
        </a>
    </div>

    <div class="contain-general-garden">
        <main>
            
            <?php
                for($i=0; $i < count($products); $i++){
            
                    if($products[$i]['available'] == 0){
            ?>

                    <div class="card">
                        <div class="no-available"> 
                            <div>indisponible</div><div>actuellement</div>
                        </div>
                        <?php
                            include('include/card.php');
                        ?>
                    </div>

            <?php
                    }else{?>
                        <div class="card">
                        <?php
                            include('include/card.php');
                        ?>
                        </div>
                        <?php
                    }
                }
                ?>
            
        </main>
        <div class="div-basket" id="div-basket">
            <div class="h-basket">
                <h2>panier</h2>
            </div>
            
            <div class="content-basket">
                <div class="products-basket" id="products-basket">

                    <div class='empty-basket'>Il n'y aucun article dans votre panier</div>
                    
                </div>
                <span class="line"></span>
                <div class="div-total">

                    <div>    
                        <h3>total</h3>
                        <span id="total-order">0</span><span>€</span>
                    </div>

                    <div id="btnPopup" class="btnPopup">commander</div>

                    <div id="overlay" class="overlay">
                        <div id="popup" class="popup">

                            <div id="btnClose" class="btnClose">&times;</div>

                            <h2>
                            Vos informations de contact
                            </h2>

                            <div class="info-form">
                            Afin de vous tenir au courrant lorsque votre panier sera prêt, nous avons besoins des informations suivantes : 
                            </div>

                            <form action="index_switch.php?controler_shop=set" method="post" class="form-info-contact">

                                <div class="div-info-contact">
                            
                                    <div class="div-line"></div>

                                    <div class="info-write">
                                        <div>
                                            <label for="name">Votre Nom</label>
                                            <input type="text" name="first-name" id="first-name" placeholder="Jones" required>
                                        </div>

                                        <div>
                                            <label for="name">Votre Prénom</label>
                                            <input type="text" name="last-name" id="last-name" placeholder="Indiana"required>
                                        </div>

                                        <div>
                                            <label for="email">Votre mail</label>
                                            <input type="email" name="email" id="email" placeholder="indiana-jones@gmail.com"required>
                                        </div>

                                        <div>
                                            <label for="tel">Votre numéro de téléphone</label>
                                            <input type="tel" name="phone" id="phone" pattern="[0-9]{10}" placeholder="0123456789"required>
                                        </div>

                                        <input id="order_input" type="hidden" name="order" value="">

                                    </div>
                                </div>
                                
                                <div class="submit-form">
                                    <input id="end-order" type="submit" value="Envoyé">
                                </div>
                                
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="scripts/basket.js"></script>
    <script type="text/javascript" src="scripts/popup.js"></script>
<?php
    include('include/footer.php');
?>