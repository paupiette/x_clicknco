<?php
    include('include/head.php');
?>

    <link href="styles/headband.css" rel="stylesheet">
    <link href="styles/home.css" rel="stylesheet">
</head>
<body>
    <header class="header-page-principale">
        <div class="block-transparence">
            <h1 class="nom-de-ferme">
                Ferme de Gally
            </h1>
        </div>
    </header>

    <?php
        include('include/nav.php');
    ?>

    <div class="home">
        <div class="contain-general-home">
            <main>
                <h4>L'histoire de Gally</h4>
                <div class="history-home">
                    <p class="p1">
                        À la Ferme de Gally, nous avons à cœur de fournir aux grands comme aux petits cuistots des légumes 
                        et des fruits bio de saisons. Depuis 4 générations d'agriculteurs, notre travail et nos produits n'ont 
                        jamais déçu les fins gourmets.
                    </p>
                    <p class="p2">
                        C'est en 1880 que Georgette Poulain achète les terres de Gally. Un domaine de 32 hectares et une cabane en ruine. Georgette, connue à l'époque pour sa force et son courage sans borne, se lance dans la rénovation de la cabane. En quelques mois, le Domaine de Gally devient la ferme de Gally.
                        Très vite, elle y plante de quoi manger : des tomates, des aubergines, des courgettes et quelques plantes aromatiques de saison.
                        Grâce à ses récoltes, les périodes fraîches sont réchauffées par de succulentes soupes de potiron.
                        Très vite, le mot se passe dans le village : "une odeur de bons légumes cuits sort toujours de la cuisine de Georgette ! ".
                        Alors ni une, ni deux Simone Potipoton décida de se rendre chez Georgette afin de connaître ses recettes.
                        Mais le secret de la soupe ne se trouva pas dans la recette, mais bien dans les ingrédients !
                        Des légumes frais, de saison, toujours ramassés au bon moment.
                        Simone, pas si bête : sait que le village raffole de ces bonnes odeurs de soupe et propose alors à Georgette de faire affaire avec elle. Georgette cultive et Simone vend !
                        Le marché est conclu, la ferme va tourner !
                    </p>
                    <p class="p2">
                        Depuis 3 autres générations ont pu rependre et améliorer le travail à la ferme de Gally.
                    </p>
                    <span class="line-history"></span>
                    <div class="p3">
                        <p>
                            Chaque année, nous tentons d'améliorer les techniques de semis et de récolte dans le respect de l'environnement pour garantir des aliments sans pesticides.
                        </p>
                    </div>
                    <span class="line-history"></span>
                    <div class="p3">
                        <p>
                            Un tiers de la terre de notre domaine est travaillée à l'aide de chevaux de trait.
                        </p>
                    </div>
                    <span class="line-history"></span>
                    <div class="p3">
                        <p> 
                            Trois quarts sont pensés en fonction des saisons.
                        </p>
                    </div>
                    <span class="line-history"></span>
                    <div class="p3">
                        <p>
                            Le quart restant est occupé par des serres dans lesquelles nous mettons au point de nouvelles variétés 
                            basées sur des espèces anciennes afin de nous permettre de nous adapter pour les changements climatiques à venir.
                        </p>
                    </div>
                    <span class="line-history"></span>
                    <div class="p3">
                        <p>
                            Un savoir ancestral avec l'envie d'améliorer l'avenir, c'est aussi ça la ferme de Gally.
                        </p>
                    </div>
                </div>
            </main>
            <aside>
                <div class="phone">
                    <h2 class="h-aside">téléphone</h2>
                    <div><?php echo $info_farm[1];?></div>
                </div>

                <span class="line-home"></span>

                <div class="hours-home">
                    <h2 class="h-aside">horaires</h2>
                    <?php
                        for($i = 0; $i <= 13; ){
                            
                            $y = $i / 2;

                            $hour_am = $hours[$i];
                            $hour_pm = $hours[$i+1];

                            $hour_am_total = $hour_am[0].$hour_am[1];
                            $hour_pm_total = $hour_pm[0].$hour_pm[1];
                           
                            ?>

                            <div class="day">
                                <h4><?php echo $day[$y]; ?></h4>

                                <div class="hour">
                                    <?php
                                        if($hour_am_total != null and $hour_pm_total != null){
                                            echo hours_string($hour_am[0]);
                                            echo " - ";
                                            echo hours_string($hour_am[1]);
                                            echo "  /  ";
                                            echo hours_string($hour_pm[0]);
                                            echo " - ";
                                            echo hours_string($hour_pm[1]);
                                        }

                                        if($hour_am_total == null and $hour_pm_total != null){
                                            echo "<span class='close'>fermé</span>";
                                            echo "  /  ";
                                            echo hours_string($hour_pm[0]);
                                            echo " - ";
                                            echo hours_string($hour_pm[1]);
                                        }

                                        if($hour_pm_total == null and $hour_am_total != null){
                                            echo hours_string($hour_am[0]);
                                            echo " - ";
                                            echo hours_string($hour_am[1]);
                                            echo "  /  ";
                                            echo "<span class='close'>fermé</span>";
                                        }

                                        if($hour_pm_total == null and $hour_am_total == null){
                                            echo "<span class='close'>fermé</span>";
                                        }
                                    ?>
                                    
                                </div>
                            </div><?php  
                            
                            $i = $i + 2;
                        }
                    ?>

                </div>
                <span class="line-home"></span>

                <div class="adress">
                    <h2 class="h-aside">adresse</h2>
                    <div><?php echo $info_farm[0];?></div>
                </div>
            </aside>
        </div> 

        <div class="pictures-home">
            <div class="carotte"></div>
            <div class="potiron"></div>
            <div class="confiture"></div>
            <div class="fruits"></div>
        </div>
    </div>


<?php
    include('include/footer.php');
?>
