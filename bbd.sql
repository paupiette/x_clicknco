drop database if exists clicknco;

create database clicknco;
grant all on clicknco.* to 'claire'@'localhost';
flush privileges;

use clicknco;

/*Table pour stocker les éléments de connexion*/
create table connexion(
    username varchar(20) not null,
    pass_word varchar(50) not null,
    unique (username, pass_word)
);

/*Table pour stocker les infos de la ferme*/
create table info_farm(
    adress varchar(100) not null,
    phone int not null
);

/*Table pour stocker les périodes d'ouvertures*/
create table time_hours(
    start_hour time,
    end_hour time,
    day_time int not null,
    check (day_time between 0 and 6)
);

/*Table pour stocker les caractéristiques des produits*/
create table product(
    id int not null auto_increment,
    primary key (id),
    name_product varchar(20) not null,
    picture_product varchar(200) not null,
    price_product decimal(5,2) not null,
    available int not null,
    check (available between 0 and 1)
);

/*Table pour stocker les commandes*/
create table one_order(
    id int not null auto_increment,
    primary key (id),
    order_status enum('validate', 'available', 'collect'),
    day_order date not null,
    client_first_name varchar(20) not null,
    client_last_name varchar(20) not null,
    client_mail varchar(35) not null,
    client_phone varchar(10) not null
);

/*Table de lien entre produits et commandes*/
create table product_order(
    quantity int not null, 
    id_order int not null,
    id_product int not null,
    foreign key (id_order) references one_order(id),
    foreign key (id_product) references product(id)
);

/*insertion dans la table info_farme*/
insert into info_farm(adress, phone) values('Ferme de Gally, 11400 Saint-Martin Lld', 1234567890);

/*insert dans la table time_hours*/
insert into time_hours(start_hour, end_hour, day_time) values('00:01:00', "00:02:00", 0);/*lundi matin*/
insert into time_hours(start_hour, end_hour, day_time) values('00:03:00', "00:04:00", 0);/*lundi aprem*/
insert into time_hours(start_hour, end_hour, day_time) values('00:05:00', "00:06:00", 1);/*mardi matin*/
insert into time_hours(start_hour, end_hour, day_time) values('00:07:00', "00:08:00", 1);/*mardi aprem*/
insert into time_hours(start_hour, end_hour, day_time) values('00:09:00', "00:10:00", 2);/*mercredi matin*/
insert into time_hours(start_hour, end_hour, day_time) values('00:11:00', "00:12:00", 2);/*mercredi aprem*/
insert into time_hours(start_hour, end_hour, day_time) values('00:13:00', "00:14:00", 3);/*jeudi matin*/
insert into time_hours(start_hour, end_hour, day_time) values('00:15:00', "00:16:00", 3);/*jeudi aprem*/
insert into time_hours(start_hour, end_hour, day_time) values('00:17:00', "00:18:00", 4);/*vendredi matin*/
insert into time_hours(start_hour, end_hour, day_time) values(null, null, 4);/*vendredi aprem*/
insert into time_hours(start_hour, end_hour, day_time) values(null, null, 5);/*samedi matin*/
insert into time_hours(start_hour, end_hour, day_time) values('00:23:00', "00:24:00", 5);/*samedi aprem*/
insert into time_hours(start_hour, end_hour, day_time) values(null, null, 6);/*dimanche matin*/
insert into time_hours(start_hour, end_hour, day_time) values(null, null, 6);/*dimanche aprem*/


insert into product(name_product, picture_product, price_product, available) values ("pommes rouges", "pictures/products/red_apple.jpg", 1.3, 0);
insert into product(name_product, picture_product, price_product, available) values ("pommes de terre", "pictures/products/potatoes.jpg", 0.8, 1);
insert into product(name_product, picture_product, price_product, available) values ("radis", "pictures/products/radishes.jpg", 1.6, 1);
insert into product(name_product, picture_product, price_product, available) values ("prunes", "pictures/products/plums.jpg", 2, 0);
insert into product(name_product, picture_product, price_product, available) values ("pêches", "pictures/products/peaches.jpg", 3.8, 1);
insert into product(name_product, picture_product, price_product, available) values ("laitue", "pictures/products/lettuce.jpg", 0.8, 0);
insert into product(name_product, picture_product, price_product, available) values ("haricots verts", "pictures/products/green_beans.jpg", 0.9, 1);
insert into product(name_product, picture_product, price_product, available) values ("choux blanc", "pictures/products/cauliflower.jpg", 3.1, 1);
insert into product(name_product, picture_product, price_product, available) values ("carottes", "pictures/products/carrots.jpg", 1, 1);
insert into product(name_product, picture_product, price_product, available) values ("abricots", "pictures/products/apricots.jpg", 4.39, 1);

insert into one_order(order_status, day_order, client_first_name, client_last_name, client_mail, client_phone) values ('validate', '2021-02-11', 'jean-jaques', 'gloman', 'jean@patrik.com', 0123456789);

insert into product_order(quantity, id_order, id_product) values(100,1,1);
select * from product;
select * from one_order;