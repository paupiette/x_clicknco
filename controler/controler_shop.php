<?php
// MODEL
	include_once('model/model_product.php');

// VIEW
	include('view/public/shop.php');

//CONTROLER : récupération des données en post
	//préparations des données à rentrer dans la base de données

	//pour récupérer la date du jour 
	$date = date('Y-m-d');

	if(isset($_POST['first-name'])){
		//informations rentré dans les input par le client
		$firstName = $_POST['first-name'];
		$lastName = $_POST['last-name'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];

		//JSON contenant les informations des produits entrés dans le panier
		$json_order = json_decode($_POST['order'], true);
		
		/*pour obtenir toutes les clés d'un tableaux
		créer un autre tableau ou les clés deviennent des valeurs 
		et sont accessible avec des clés numérotées à partir de 0*/
		$key = array_keys($json_order);

		$order = array();

		for($i=0; $i < count($json_order); $i++){
			/*pour récupérer 'décoder' le JSON de chaque produit 
			qui avait été entré dans le sessionStorage - tableau associatif en sortie*/	
			$value = json_decode($json_order[$key[$i]], true);
			/*on ajoute la valeur récupérer au tableau $order*/
			array_push($order, $value);
		}
	}

//MODEL : envois de la commande en bdd
	include('model/model_send_order.php');
?>